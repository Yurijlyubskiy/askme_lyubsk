from django import forms
from app.models import Question, Profile, Answer
from django.contrib.auth.models import User


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())


class SignupForm(forms.ModelForm):
    Login = forms.CharField()
    Nickname = forms.CharField()
    Email = forms.EmailField()
    Password = forms.CharField(widget=forms.PasswordInput())
    RepeatPassword = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = Profile
        fields = ['avatarlink']

    def clean(self):
        cleaned_data = super().clean()
        password_one = cleaned_data['Password']
        password_two = cleaned_data['RepeatPassword']
        if password_one != password_two:
            self.add_error(None, 'passwords do not match')
        return cleaned_data


class SettingsForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'username', 'last_name', 'password']

    RepeatPassword = forms.CharField(widget=forms.PasswordInput())
    Avatar_Link = forms.CharField()

    def clean(self):
        cleaned_data = super().clean()
        password_one = cleaned_data['password']
        password_two = cleaned_data['RepeatPassword']
        if password_one != password_two:
            self.add_error(None, 'passwords do not match')
        return cleaned_data


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['title', 'text', 'fk_tags']


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['text']
