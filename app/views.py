from django.shortcuts import render, redirect, Http404, reverse
from django.contrib import auth
from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.decorators import login_required
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from app.models import Question
from app.models import Tag
from app.models import Profile
from app.models import Answer
from app.forms import LoginForm, QuestionForm, SignupForm, SettingsForm, AnswerForm


questions = [
    {
        'id': idx,
        'title': f'Title number {idx}',
        'text': f'Some text for question #{idx}'
    } for idx in range(30)
]


def paginate(objects_list, request, per_page=5):
    paginator = Paginator(objects_list, per_page)
    page = request.GET.get('page')
    iterators = paginator.get_page(page)
    return iterators


def hot_questions(request):
    request.session['continue'] = reverse('hot_questions')
    contact_list = Question.objects.get_hot()
    iterators = paginate(contact_list, request, 5)
    return render(request, 'hot.html', {'iterators': iterators, 'url': "/new/"})


def new_questions(request):
    request.session['continue'] = reverse('new_questions')
    contact_list = Question.objects.get_new()
    iterators = paginate(contact_list, request, 5)
    return render(request, 'new.html', {'iterators': iterators,   'url': "/hot/"})


@login_required
def ask_question(request):
    request.session['continue'] = reverse('ask')
    if request.method == 'GET':
        form = QuestionForm
    else:
        form = QuestionForm(data=request.POST)
        if form.is_valid():
            question = form.save(commit=False)
            question.fk_profile = request.user.profile
            question.save()
            question.fk_tags.set(form.cleaned_data['fk_tags'])
            question.save()
            return redirect(reverse('one_question', args=[question.pk]))

    return render(request, 'ask.html', {'form': form})


def signup(request):
    if request.method == 'GET':
        form = SignupForm
        form.avatarlink = './img/210.jpg/'
    else:
        form = SignupForm(data=request.POST)
        if form.is_valid():
            user = User.objects.create_user(form.cleaned_data['Login'], form.cleaned_data['Email'], form.cleaned_data['Password'])
            if user is not None:
                user.last_name = form.cleaned_data['Nickname']
                profile = Profile.objects.create(user=user)
                profile.avatarlink = form.cleaned_data['avatarlink']
                profile.save()
                user.save()
                auth.login(request, user)
                # main page
                return redirect(reverse('new_questions'))
            else:
                form.add_error(None, 'User already exists')

    return render(request, 'register.html', {'form': form})


def mylogin(request):
    if request.method == 'GET':
        form = LoginForm
    else:
        form = LoginForm(data=request.POST)
        if form.is_valid():
            user = auth.authenticate(request, **form.cleaned_data)
            if user is not None:
                auth.login(request, user)
                nexturl = request.session.pop('continue', '/new/')
                return redirect(nexturl)
            else:
                form.add_error(None, 'Check login and password')

    return render(request, 'login.html', {'form': form})


@login_required
def settings(request):
    if request.method == 'GET':
        form = SettingsForm
    else:
        form = SettingsForm(data=request.POST)
        if form.is_valid():
            profile = Profile.objects.get(user=request.user)
            profile.avatarlink = form.cleaned_data['Avatar_Link']
            profile.save()
            nexturl = request.session.pop('continue', reverse('new_questions'))
            return redirect(nexturl)
        else:
            form.add_error(None, 'General Error')

    return render(request, 'settings.html', {'form': form})


def logout(request):
    nexturl = request.session.pop('continue', '/new/')
    auth.logout(request)
    return redirect(nexturl)


def one_question(request, pk):
    question = Question.objects.get_by_id(pk=pk)
    request.session['continue'] = reverse('one_question', args=[question.pk])
    iterators = paginate(Answer.objects.get_by_question_id(pk), request, 5)
    request.session['continue'] = reverse('ask')

    if request.method == 'GET':
        form = AnswerForm
    else:
        form = AnswerForm(data=request.POST)
        if form.is_valid():
            answer = form.save(commit=False)
            answer.fk_profile = request.user.profile
            answer.fk_question = question
            answer.save()
            page = iterators.end_index()
            return redirect(reverse('one_question', args=[question.pk]) + '?page=' + str(page) + '#' + str(answer.id))
    return render(request, 'question.html', {'question': question, 'iterators': iterators, 'form': form})


def tags(request, tg):
    request.session['continue'] = reverse('tags', args=[tg])
    quest_list = Question.objects.get_by_tag(tg)
    iterators = paginate(quest_list, request, 5)

    return render(request, 'tag.html', {'iterators': iterators, 'header': tg, })
