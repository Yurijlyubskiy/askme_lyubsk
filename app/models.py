from django.db import models
from django.db.models import ObjectDoesNotExist
from django.db.models import Sum, Count
from django.http import HttpResponse, Http404
from django.contrib.auth.models import User

# Create your models here.


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=None)
    avatarlink = models.CharField(max_length=255,
                                  default='./img/210.jpg')

    def __str__(self):
        if self.user.username == '':
            return "ERROR-LOGIN IS NULL"
        return self.user.username

    class Meta:
        verbose_name = 'User profile'
        verbose_name_plural = 'User profiles'

    @property
    def userlink(self):
        return "/user/" + self.user.username


class TagManager(models.Manager):

    def get_by_question(self, question):
        try:
            answers = self.all().prefetch_related('question_set').filter(question=question)
        except ObjectDoesNotExist:
            raise Http404
        return answers


class Tag(models.Model):

    def __str__(self):
        if self.tagname == '':
            return "ERROR-TAG NAME IS NULL"
        return self.tagname

    class Meta:
        db_table = 'tags'
        managed = True
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'

    tagname = models.CharField(primary_key=True, max_length=255)

    objects = TagManager()

    @property
    def link(self):
        return "/tags/" + self.tagname


class QuestionManager(models.Manager):
    def get_new(self):
        return self.select_related().order_by("-date").prefetch_related('fk_profile', 'fk_tags')

    def get_hot(self):
        return self.select_related().order_by("-hidden_rating").prefetch_related('fk_profile')

    def get_by_tag(self, tag):
        questions = self.filter(fk_tags__tagname__iexact=tag).prefetch_related('fk_profile')
        if not questions:
            raise Http404
        return questions

    def get_by_id(self, pk):
        try:
            questions = self.prefetch_related('fk_profile', 'fk_tags').get(pk=pk)
        except ObjectDoesNotExist:
            raise Http404
        return questions


class Question(models.Model):

    def __str__(self):
        if self.title == '':
            return "ERROR-QUESTION TITLE IS NULL"
        return self.title

    objects = QuestionManager()

    fk_profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    fk_tags = models.ManyToManyField(Tag)
    hidden_rating = models.IntegerField(default=0)
    _answers_num = models.IntegerField(default=0)
    title = models.CharField(max_length=255)
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)

    @property
    def link(self):
        return "/question/" + str(self.id) + "/"

    @property
    def answers_num(self):
        _answers_num = Answer.objects.all().filter(fk_question=self.id).count()
        return _answers_num

    @property
    def rating(self):
        q = QuestionRatingMark.objects.all().filter(fk_question=self.id)
        sum = 0
        for i in q:
            sum += i.vote
        hidden_rating = sum
        return hidden_rating

    class Meta:
        db_table = 'questions'
        managed = True
        verbose_name = 'Question'
        verbose_name_plural = 'Questions'


class AnswerManager(models.Manager):
    def get_new(self):
        return self.all().order_by('-date').prefetch_related('user')

    def get_by_question_id(self, q_id):
        try:
            answers = self.all().prefetch_related('fk_profile').filter(fk_question=q_id)
        except ObjectDoesNotExist:
            raise Http404
        return answers


class Answer(models.Model):

    def __str__(self):
        if self.text == None:
            return "ERROR-ANSWER TEXT IS NULL"
        return self.text[:255]


    class Meta:
        db_table = 'answers'
        managed = True
        verbose_name = 'Answer'
        verbose_name_plural = 'Answers'

    objects = AnswerManager()

    fk_question = models.ForeignKey(Question, on_delete=models.CASCADE)
    fk_profile = models.ForeignKey(Profile, on_delete=models.CASCADE)

    _rating = models.IntegerField(default=0)
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    marked_correct = models.BooleanField(default=False)

    @property
    def rating(self):
        q = AnswerRatingMark.objects.all().filter(fk_answer=self.id)
        sum = 0
        for i in q:
            sum += i.vote
        _rating = sum
        return _rating



class QuestionRatingMark(models.Model):
    votes = [(1, 'like'), (-1, 'dislike'), (0, 'none'), ]

    vote = models.IntegerField(choices=votes, default=0)
    fk_profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    fk_question = models.ForeignKey(Question, on_delete=models.CASCADE)

    def __str__(self):
        return self.fk_question.title + '-' + self.fk_profile.user.username + ': ' + str(self.vote)

    class Meta:
        verbose_name = 'Q-VoteMark'
        verbose_name_plural = 'Q-VoteMarks'
        unique_together = ['fk_profile', 'fk_question']

    def update_rating(self):
        self.question.rating += self.vote


class AnswerRatingMark(models.Model):
    votes = [(1, 'like'), (-1, 'dislike'), (0, 'none'), ]
    vote = models.IntegerField(choices=votes, default=0)
    fk_profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    fk_answer = models.ForeignKey(Answer, on_delete=models.CASCADE)

    def __str__(self):
        return self.fk_answer.text[:10] + '-' + self.fk_profile.user.username + ': ' + str(self.vote)

    class Meta:
        verbose_name = 'A-VoteMark'
        verbose_name_plural = 'A-VoteMarks'
        unique_together = ['fk_profile', 'fk_answer']
